(function ($) {

Drupal.behaviors.font_awesome_example = {
  attach: function (context, settings) {
    // remove the active class just for the demo
    $('.content .active:not(.keep-active)', context).removeClass('active');
  }
};

})(jQuery);
